"""
Implementation of Kruskal's algorithm
"""


class Kruskal():
    """
    Implementation of Kruskal's algorithm
    """
    def __init__(self):
        self.edges = None
        self.min_ost = None
        self.weight_min_ost = None
        self.parent = None
        self.rank = None

    def parse(self, path: str, filename: str) -> bool:
        """
        Parse graph from file
        :param path: pathway to file
        :param filename: name of file with graph
        :return: Boolean read success signal
        """
        self.edges = []
        self.parent = []
        self.rank = []
        self.min_ost = None
        self.weight_min_ost = None
        try:
            with open(path+filename) as file:
                count_nodes = int(file.readline().split()[0])
                self.parent = list(range(count_nodes))
                self.rank = [0 for _ in range(count_nodes)]
                for line in file:
                    if len(line) < 5:
                        continue
                    edge = tuple(map(int, line.split()))
                    self.edges.append(edge[::-1])
            return True
        except FileNotFoundError:
            self.edges = None
            self.parent = None
            self.rank = None
            return False

    def fit(self) -> bool:
        """
        Search for the spanning tree
        :return: Boolean signal about the success of the search
        """
        if self.min_ost:
            return True
        if self.edges is None:
            return False

        self.edges.sort()
        self.weight_min_ost = 0
        self.min_ost = set()
        for weight, end, start in self.edges:
            _start, _end = self._find_set(start), self._find_set(end)
            if _start == _end:
                continue
            self.weight_min_ost += weight
            self.min_ost.add((start, end))
            self._union_sets(_start, _end)
        return True

    def get_min_ost(self) -> set:
        """
        Get spanning tree
        :return: edges of spanning tree
        """
        return self.min_ost

    def get_weight_min_ost(self) -> int:
        """
        Get weight of spanning tree
        :return: weight of spanning tree
        """
        return self.weight_min_ost

    def _find_set(self, node):
        if node == self.parent[node]:
            return node
        self.parent[node] = self._find_set(self.parent[node])
        return self.parent[node]

    def _union_sets(self, start, end):
        start = self._find_set(start)
        end = self._find_set(end)
        if start != end:
            if self.rank[start] < self.rank[end]:
                start, end = end, start
            self.parent[end] = start
            if self.rank[start] == self.rank[end]:
                self.rank[start] += 1
