from kruskal import Kruskal

import unittest


TEST_PATHWAY = 'test_data/'


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.kr = Kruskal()

    def empty_test(self):
        self.kr.parse(TEST_PATHWAY, '1')
        self.kr.fit()
        self.assertSetEqual(self.kr.get_min_ost(), set())
        self.assertEqual(self.kr.get_weight_min_ost(), 0)

    def tree_test(self):
        self.kr.parse(TEST_PATHWAY, '2')
        self.kr.fit()
        ref = set([(0, 1), (0, 2), (1, 3), (2, 4), (4, 5),
                   (4, 6), (5, 7), (5, 8), (3, 9)])
        self.assertSetEqual(self.kr.get_min_ost(), ref)
        self.assertEqual(self.kr.get_weight_min_ost(), 24)

    def some_graph_test(self):
        self.kr.parse(TEST_PATHWAY, '3')
        self.kr.fit()
        ref = set([(0, 1), (0, 2), (1, 3), (2, 4), (4, 5),
                   (4, 6), (5, 7), (5, 8), (3, 9)])
        self.assertSetEqual(self.kr.get_min_ost(), ref)
        self.assertEqual(self.kr.get_weight_min_ost(), 24)

    def not_one_result_test(self):
        self.kr.parse(TEST_PATHWAY, '4')
        self.kr.fit()
        ref1 = set([(0, 1), (0, 2), (1, 3), (2, 4), (4, 5),
                    (4, 6), (5, 7), (5, 8), (3, 9)])
        ref2 = set([(0, 1), (0, 2), (1, 3), (2, 4), (4, 5),
                    (4, 6), (7, 8), (5, 8), (3, 9)])
        res = self.kr.get_min_ost()
        self.assertTrue(res == ref1 or res == ref2)
        self.assertEqual(self.kr.get_weight_min_ost(), 24)


if __name__ == '__main__':
    unittest.main()
